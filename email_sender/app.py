from email.message import EmailMessage
from flask import Flask, request
import smtplib
import os

app = Flask(__name__)

@app.route('/')
def index():
    return "Email Sending App"

@app.route('/email', methods=["POST"])
def form():
    email_receiver = "dumiandrei99@gmail.com"
    password = os.environ.get('EMAIL')

    email_sender = request.form.get('email_sender')
    message_sent = request.form.get('message_sent')

    print(email_sender)
    print(message_sent)

    if len(email_sender) < 1: 
        return "You must complete your email before sending a message to us!"
    
    if len(message_sent) < 1 :
        return "You can't send an empty message to us!"

    email_content = "Sender: " + email_sender + "\nYou receieved the following message: " + message_sent

    email_message = EmailMessage()
    email_message['Subject'] = "Somebody sent you a message via your website!"
    email_message['From'] = email_sender
    email_message['To'] = email_receiver
    email_message.set_content(email_content)

    server = smtplib.SMTP("smtp.gmail.com", 587)
    server.starttls()
    server.login(email_receiver, password)
    server.send_message(email_message)
    server.quit()
    
    return "Email sent!"

if __name__ == "__main__":
    app.run(debug=True)