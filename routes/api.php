<?php

use App\Models\User;
use App\Models\Location;
use App\Models\LocationView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Hash;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/users', function () {
    return User::all();
});

// login route

Route::post('/log-in', function () {
    if (is_null(request('username')) || is_null(request('password'))) {
        $response = array(
            "response" => "All fields are mandatory !",
            "status" => "207"
        );

        return $response;
    }

    $username = request('username');
    $password = request('password');

    $userByUsername = User::where('name', $username)->first();

    if (is_null($userByUsername)) {
        $response = array(
            "response" => "Username doesn't exist !",
            "status" => "206",
        );

        return $response;
    }

    if ($userByUsername->isAdmin) {
        $userType = 'admin';
    } else {
        $userType = 'regular';
    }

    if ($userType === 'admin') {
        if ($password === $userByUsername->password) {
            $response = array(
                "userType" => $userType,
                "status" => "200"
            );
            return $response;
        } else {
            $response = array(
                "response" => "The password you entered is wrong !",
                "status" => "205"
            );
            return $response;
        }
    }


    if (Hash::check($password, $userByUsername->password)) {

        $response = array(
            "userType" => $userType,
            "status" => "200"
        );
        return $response;
    } else {
        $response = array(
            "response" => "Wrong entered password!",
            "status" => "205"
        );
        return $response;
    }
});

// register route
Route::post('/create-user', function () {

    if (is_null(request('name')) || is_null(request('email')) || is_null(request('password'))) {
        $response = array(
            "response" => "All fields are mandatory !",
            "status" => "203"
        );

        return $response;
    }

    $name = request('name');
    $email = request('email');
    $password = Hash::make(request('password'));

    $user = User::where('name', $name)->first();

    if (!is_null($user)) {
        $response = array(
            "response" => "Username already exists!",
            "status" => "202"
        );

        return $response;
    }

    $user = User::where('email', $email)->first();

    if (!is_null($user)) {
        $response = array(
            "response" => "Email already exists!",
            "status" => "201"
        );

        return $response;
    }

    User::create([
        'name' => $name,
        'email' => $email,
        'password' => $password,
        'isAdmin' => false
    ]);

    $response = array(
        "response" => "You successfully registered!",
        "status" => "200"
    );

    return $response;
});


// get all locations route
Route::get('/locations', function () {
    return Location::all();
});

// get a location based on it's id
Route::get('/location/{id}', function () {
    $id = Route::getCurrentRoute()->id;
    return Location::where('id', $id)->first();
});

// creates a location view entry
Route::post('/create-location-view', function () {

    if (is_null(request('location_id')) || is_null(request('hour')) || is_null(request('date'))) {
        $response = array(
            "response" => "ID, hour and date fields are mandatory !",
            "status" => "203"
        );

        return $response;
    }

    $locationId = request('location_id');
    $hour = request('hour');
    $date = request('date');

    $locationView = LocationView::where([
        ['location_id', $locationId],
        ['hour', $hour],
        ['date', $date]
    ])->first();

    if (!is_null($locationView)) {
        $response = array(
            "response" => "This viewing is booked at this date and time !",
            "status" => "204"
        );

        return $response;
    }

    LocationView::create([
        'location_id' => $locationId,
        'date' =>  $date,
        'hour' => $hour
    ]);

    $response = array(
        "response" => "Successfully booked the view of property $locationId!",
        "status" => "200",
    );

    return $response;
});

// creates a location
Route::post('/create-location', function () {

    if (is_null(request('description')) || is_null(request('type')) || is_null(request('saleOrRent')) || is_null(request('price'))) {
        $response = array(
            "response" => "All fields are mandatory !",
            "status" => "203"
        );

        return $response;
    }

    $description = request('description');
    $type = request('type');
    $saleOrRent = request('saleOrRent');
    $price = request('price');

    if ($type != 'residential' && $type != 'comercial') {
        $response = array(
            "response" => "Type: Only 'residential' or 'comercial' allowed (lower case) !",
            "status" => "203"
        );

        return $response;
    }


    if ($saleOrRent !== 'sale' && $saleOrRent !== 'rent') {
        $response = array(
            "response" => "Sale or Rent: Only 'sale' or 'rent' allowed (lower case) !",
            "status" => "203"
        );

        return $response;
    }

    if (!is_numeric($price)) {
        $response = array(
            "response" => "Only numeric values allowed for Price !",
            "status" => "203"
        );

        return $response;
    } else {
        $price = intval($price);
    }

    $isForSale = false;
    $isForRent = false;

    $saleOrRent === 'sale' ? $isForSale = true : $isForRent = true;

    Location::create([
        'description' => $description,
        'price' => $price,
        'isForSale' => $isForSale,
        'isForRent' => $isForRent,
        'type' => $type
    ]);

    $response = array(
        "response" => "Successfully added the property",
        "status" => "200",
    );

    return $response;
});

Route::post('/get-location-views', function () {

    if (is_null(request('id')) || is_null(request('date'))) {
        $response = array(
            "response" => "All fields are mandatory !",
            "status" => "203"
        );

        return $response;
    }

    $locationId = request('id');
    $date = request('date');

    return LocationView::where([
        ['location_id', $locationId],
        ['date', $date]
    ])->get();
});
